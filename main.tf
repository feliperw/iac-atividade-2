terraform {

  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.40.0"
    }
  }
}

provider "aws" {
  alias  = "east"
  region = "us-east-1"
}

provider "aws" {
  alias  = "west"
  region = "us-west-2"
}

module "east" {
  source = "./modules/east"

  vpc_cidr      = "10.0.0.0/16"
  sn1_cidr      = "10.0.1.0/24"
  sn2_cidr      = "10.0.2.0/24"
  sn3_cidr      = "10.0.3.0/24"
  instance_type = "t2.micro"
  ami           = "ami-09a41e26df464c548"

  providers = {
    aws = aws.east
  }
}

module "west" {
  source = "./modules/west"

  vpc_cidr     = "10.1.0.0/16"
  sn1_cidr     = "10.1.1.0/24"
  sn2_cidr     = "10.1.2.0/24"
  sn3_cidr     = "10.1.3.0/24"
  rds_username = "iac"
  rds_password = "IacMack23"

  providers = {
    aws = aws.west
  }
}