# IAC - Atividade 2

## Objetivo:
- Criar um módulo terraform que crie e em multi regiões (2) os seguintes recursos:
  - VPC (2 vps, uma em cada região)
  - 3 subnets (uma em cada AZ) - para cada VPC
  - Internet Gateway para a VPC apenas
  - 2 maquinas EC2 em cada região (apenas em uma vpc)
  - 1 banco de dados RDS PostgreSQL (apenas em uma vpc)
  - 1 balanceador de carga (para as ec2s)
  - Security Group para cada recurso acima (EC2, RDS e Balanceador)

## Pré Reqs:
- Instalar o Terraform (https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli) na maquina onde será orquestrado o provisionamento da infra
- Ter um usuario programatico na AWS (https://docs.aws.amazon.com/pt_br/IAM/latest/UserGuide/id_users_create.html)

## Execucão:
- Baixar os arquivos deste repositório:
```
git clone https://gitlab.com/feliperw/iac-atividade-2.git
```
	
- Acesse o diretório clonado e crie a chave a ser utilizada nas Instâncias da AWS:
```
cd iac-atividade-2
mkdir keys
ssh-keygen -f keys/aws
```

- Exporte as seguintes variáveis de ambiente:
```
export AWS_ACCESS_KEY_ID="{Insira o ID do usuário programático da AWS criado nos passos anteriores}"
export AWS_SECRET_ACCESS_KEY="{Insira a Secret do usuário programático da AWS criado nos passos anteriores}"
```

- Execute os comandos do Terraform para criar a infra:
```
terraform init
terraform plan
terraform apply
```

- Após a criação da infra, será exibido o IP de cada VM, endereço de DNS do LB e endpoint do RDS

```
rds-iac-west-01-endpoint = ""
vm-iac-east-01-ip = "X.X.X.X"
vm-iac-east-02-ip = "X.X.X.X"
lb-iac-east-01-dns = ""
```


