variable "vpc_cidr" {
  default     = ""
  type        = string
  description = "CIDR for VPC"
}

variable "sn1_cidr" {
  default     = ""
  type        = string
  description = "CIDR for Subnet"
}

variable "sn2_cidr" {
  default     = ""
  type        = string
  description = "CIDR for Subnet"
}

variable "sn3_cidr" {
  default     = ""
  type        = string
  description = "CIDR for Subnet"
}

variable "rds_username" {
  default     = ""
  type        = string
  description = "Username for RDS"
}

variable "rds_password" {
  default     = ""
  type        = string
  description = "Password for RDS"
}