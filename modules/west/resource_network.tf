resource "aws_vpc" "vpc_iac_west_01" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "vpc-iac-west-01"
  }
}

resource "aws_subnet" "sn_iac_west_01" {
  vpc_id            = aws_vpc.vpc_iac_west_01.id
  cidr_block        = var.sn1_cidr
  availability_zone = "us-west-2a"

  tags = {
    Name = "sn-iac-west-01"
  }
}

resource "aws_subnet" "sn_iac_west_02" {
  vpc_id            = aws_vpc.vpc_iac_west_01.id
  cidr_block        = var.sn2_cidr
  availability_zone = "us-west-2b"

  tags = {
    Name = "sn-iac-west-02"
  }
}

resource "aws_subnet" "sn_iac_west_03" {
  vpc_id            = aws_vpc.vpc_iac_west_01.id
  cidr_block        = var.sn3_cidr
  availability_zone = "us-west-2c"

  tags = {
    Name = "sn-iac-west-03"
  }
}

resource "aws_internet_gateway" "ig_iac_west_01" {
  vpc_id = aws_vpc.vpc_iac_west_01.id

  tags = {
    Name = "ig-iac-west-01"
  }
}

resource "aws_route_table" "rt_iac_west_01" {
  vpc_id = aws_vpc.vpc_iac_west_01.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig_iac_west_01.id
  }

  tags = {
    Name = "rt-iac-west-01"
  }
}

resource "aws_route_table_association" "rta_iac_west_01" {
  subnet_id      = aws_subnet.sn_iac_west_01.id
  route_table_id = aws_route_table.rt_iac_west_01.id
}

resource "aws_route_table_association" "rta_iac_west_02" {
  subnet_id      = aws_subnet.sn_iac_west_02.id
  route_table_id = aws_route_table.rt_iac_west_01.id
}

resource "aws_route_table_association" "rta_iac_west_03" {
  subnet_id      = aws_subnet.sn_iac_west_03.id
  route_table_id = aws_route_table.rt_iac_west_01.id
}

resource "aws_security_group" "sg_rds_west_01" {
  name        = "sg_rds_west_01"
  description = "Allow traffic"
  vpc_id      = aws_vpc.vpc_iac_west_01.id

  ingress {
    description = "ALL to RDS"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg-rds-west-01"
  }
}
