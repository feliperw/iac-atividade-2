resource "aws_db_subnet_group" "rdssn_iac_west_01" {
  name       = "rdssn-iac-west-01"
  subnet_ids = [aws_subnet.sn_iac_west_01.id, aws_subnet.sn_iac_west_02.id, aws_subnet.sn_iac_west_03.id]

  tags = {
    Name = "rdssn-iac-west-01"
  }
}

resource "aws_db_instance" "rds_iac_west_01" {
  allocated_storage      = 10
  db_name                = "rds_iac_west_01"
  db_subnet_group_name   = aws_db_subnet_group.rdssn_iac_west_01.name
  vpc_security_group_ids = [aws_security_group.sg_rds_west_01.id]
  engine                 = "postgres"
  instance_class         = "db.t3.micro"
  username               = var.rds_username
  password               = var.rds_password
  skip_final_snapshot    = true
}
