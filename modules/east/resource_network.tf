resource "aws_vpc" "vpc_iac_east_01" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "vpc-iac-east-01"
  }
}

resource "aws_subnet" "sn_iac_east_01" {
  vpc_id            = aws_vpc.vpc_iac_east_01.id
  cidr_block        = var.sn1_cidr
  availability_zone = "us-east-1a"

  tags = {
    Name = "sn-iac-east-01"
  }
}

resource "aws_subnet" "sn_iac_east_02" {
  vpc_id            = aws_vpc.vpc_iac_east_01.id
  cidr_block        = var.sn2_cidr
  availability_zone = "us-east-1b"

  tags = {
    Name = "sn-iac-east-02"
  }
}

resource "aws_subnet" "sn_iac_east_03" {
  vpc_id            = aws_vpc.vpc_iac_east_01.id
  cidr_block        = var.sn3_cidr
  availability_zone = "us-east-1c"

  tags = {
    Name = "sn-iac-east-03"
  }
}

resource "aws_internet_gateway" "ig_iac_east_01" {
  vpc_id = aws_vpc.vpc_iac_east_01.id

  tags = {
    Name = "ig-iac-east-01"
  }
}

resource "aws_route_table" "rt_iac_east_01" {
  vpc_id = aws_vpc.vpc_iac_east_01.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ig_iac_east_01.id
  }

  tags = {
    Name = "rt-iac-east-01"
  }
}

resource "aws_route_table_association" "rta_iac_east_01" {
  subnet_id      = aws_subnet.sn_iac_east_01.id
  route_table_id = aws_route_table.rt_iac_east_01.id
}

resource "aws_route_table_association" "rta_iac_east_02" {
  subnet_id      = aws_subnet.sn_iac_east_02.id
  route_table_id = aws_route_table.rt_iac_east_01.id
}

resource "aws_route_table_association" "rta_iac_east_03" {
  subnet_id      = aws_subnet.sn_iac_east_03.id
  route_table_id = aws_route_table.rt_iac_east_01.id
}

resource "aws_security_group" "sg_lb_east_01" {
  name        = "sg_lb_east_01"
  description = "Allow traffic"
  vpc_id      = aws_vpc.vpc_iac_east_01.id

  ingress {
    description = "HTTP to LB"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg-lb-east-01"
  }
}

resource "aws_security_group" "sg_vms_east_01" {
  name        = "sg_vms_east_01"
  description = "Allow traffic"
  vpc_id      = aws_vpc.vpc_iac_east_01.id

  ingress {
    description = "SSH to Instance"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description     = "HTTP to Instance"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_lb_east_01.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sg-vms-east-01"
  }
}
