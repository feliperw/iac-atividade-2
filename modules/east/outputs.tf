output "vm-iac-east-01-ip" {
  value = aws_instance.vm-iac-east-01.public_ip
}

output "vm-iac-east-02-ip" {
  value = aws_instance.vm-iac-east-02.public_ip
}

output "lb-iac-east-01-dns" {
  value = aws_lb.lb_iac_east_01.dns_name
}