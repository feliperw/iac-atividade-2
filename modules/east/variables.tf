variable "vpc_cidr" {
  default     = ""
  type        = string
  description = "CIDR for VPC"
}

variable "sn1_cidr" {
  default     = ""
  type        = string
  description = "CIDR for Subnet"
}

variable "sn2_cidr" {
  default     = ""
  type        = string
  description = "CIDR for Subnet"
}

variable "sn3_cidr" {
  default     = ""
  type        = string
  description = "CIDR for Subnet"
}

variable "instance_type" {
  default     = "t2.micro"
  type        = string
  description = "Instance type for instance"
}

variable "ami" {
  default     = true
  type        = string
  description = "AMI for instance"
}