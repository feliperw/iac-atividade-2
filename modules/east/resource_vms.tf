resource "aws_key_pair" "key-iac-east-01" {
  key_name   = "key-iac-east-01"
  public_key = file("./keys/aws.pub")
}

resource "aws_instance" "vm-iac-east-01" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.key-iac-east-01.key_name
  subnet_id                   = aws_subnet.sn_iac_east_01.id
  vpc_security_group_ids      = [aws_security_group.sg_vms_east_01.id]
  associate_public_ip_address = true

  tags = {
    Name = "vm-iac-east-01"
  }
}

resource "aws_instance" "vm-iac-east-02" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.key-iac-east-01.key_name
  subnet_id                   = aws_subnet.sn_iac_east_02.id
  vpc_security_group_ids      = [aws_security_group.sg_vms_east_01.id]
  associate_public_ip_address = true
  tags = {
    Name = "vm-iac-east-02"
  }
}