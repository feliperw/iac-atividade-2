resource "aws_lb" "lb_iac_east_01" {
  name               = "lb-iac-east-01"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.sg_lb_east_01.id]
  subnets            = [aws_subnet.sn_iac_east_01.id, aws_subnet.sn_iac_east_02.id, aws_subnet.sn_iac_east_03.id]
}

resource "aws_lb_target_group" "lbtg_iac_east_01" {
  name     = "lbtg-iac-east-01"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc_iac_east_01.id
}

resource "aws_lb_target_group_attachment" "lbtga_iac_east_01" {
  target_group_arn = aws_lb_target_group.lbtg_iac_east_01.arn
  target_id        = aws_instance.vm-iac-east-01.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "lbtga_iac_east_02" {
  target_group_arn = aws_lb_target_group.lbtg_iac_east_01.arn
  target_id        = aws_instance.vm-iac-east-02.id
  port             = 80
}

resource "aws_lb_listener" "lbl_iac_east_01" {
  load_balancer_arn = aws_lb.lb_iac_east_01.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lbtg_iac_east_01.arn
  }
}