output "rds-iac-west-01-endpoint" {
  value = "${module.west.rds-iac-west-01-endpoint}"
}

output "vm-iac-east-01-ip" {
  value = "${module.east.vm-iac-east-01-ip}"
}

output "vm-iac-east-02-ip" {
  value = "${module.east.vm-iac-east-02-ip}"
}

output "lb-iac-east-01-dns" {
  value = "${module.east.lb-iac-east-01-dns}"
}
